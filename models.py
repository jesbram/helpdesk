from django.db import models
from django.contrib.auth.models import User
try:
    from django.db.models import JSONField
except ImportError:
    from django.contrib.postgres.fields import JSONField
from django.conf import settings
from datetime import datetime
# Create your models here.
class Theme(models.Model):
	name=models.CharField("Nombre",max_length=250,unique=True)
	image=models.FileField("Archivo",upload_to="helpdesk/images/")
	description=models.TextField("Descripcion")


class Package(models.Model):
	name=models.CharField("Nombre",max_length=250)
	codename=models.CharField("Codigo",max_length=250,unique=True)
	description=models.TextField("Descripcion")
	options=JSONField("Opciones")

class Template(models.Model):
	CURRENCY=[
		("usd","Dolar"),
		("eur","Euro"),
	]
	RECURRENCY=[
		("hour","Por Hora"),
		("day","Diario"),
		("15-31","Quience y ultimo"),
		("month","Mensual"),
		("3-month","Trimestral"),
		("6-month","Semestral"),
		("year","Anual"),
	]
	price=models.IntegerField("Precio")
	currency=models.CharField("Moneda",
		max_length=250,
		choices=CURRENCY,default="usd")
	recurrency=models.CharField("Recurrencia",max_length=250,
		help_text="Ciclo de facturacion ",
		choices=RECURRENCY,default="month")
	package=models.ForeignKey(Package,on_delete=models.CASCADE)
	def __str__(self):
		return f"Template[{self.id}]:{self.package.codename}"

class Service(models.Model):
	"""
	Es el servicio el cual estara asignado a un usuario
	por defecto el nombre del servicio es el nombre de 

	su plantilla dado que el servicio esta asignado al usuario
	un mismo servicio puede tener diferente precio dependiendo 
	del usuario 

	La definicion de precios en la 
	"""
	CURRENCY=[
		("usd","Dolar"),
		("eur","Euro"),
	]
	RECURRENCY=[
		("hour","Por Hora"),
		("day","Diario"),
		("15-31","Quience y ultimo"),
		("month","Mensual"),
		("3-month","Trimestral"),
		("6-month","Semestral"),
		("year","Anual"),
	]
	STATUS=[
		("active","Activo"),
		("suspend","Suspendido"),
		("cancel","Cancelado"),
	]
	price=models.IntegerField("Precio",
		blank=True,null=True,
		default=None)
	currency=models.CharField("Moneda",
		blank=True,null=True,
		max_length=250,
		default=None,
		choices=CURRENCY)
	status=models.CharField("Status",
		max_length=260,
		choices=STATUS,default="active")
	recurrency=models.CharField("Recurrencia",
		max_length=250,
		blank=True,null=True,
		help_text="Ciclo de facturacion ",
		choices=RECURRENCY,
		default=None)
	name=models.CharField("Nombre",max_length=250,blank=True,null=True,unique=True)
	start_datetime=models.DateTimeField("Fecha inicio")
	suspend_datetime=models.DateTimeField("Tiempo suspendido",blank=True,null=True)
	end_datetime=models.DateTimeField("Fecha Fin",blank=True,null=True)
	user=models.ForeignKey(User,on_delete=models.CASCADE)
	template=models.ForeignKey(Template,on_delete=models.CASCADE)
	description=models.TextField("Descripcion")

	@staticmethod
	def get_base_url(request,base_url):
		if "domain"  in request.COOKIES and settings.HELPDESK_REDIRECT_SERVICE: 
			return "https://"+request.COOKIES["domain"]
		return base_url
	def __str__(self):
		return f"Service[{self.id}]: {self.name} | {self.user}"
class ServiceMeta(models.Model):
	"""
	"""
	service=models.ForeignKey(Service,on_delete=models.CASCADE)
	key=models.CharField(max_length=250)
	value=JSONField(blank=True,null=True) if settings.JSON_COMPATIBLE else models.TextField(blank=True,null=True,default="null") 
	encrypted=models.BooleanField(default=False)
	description=models.TextField(blank=True,null=True,
		help_text="""Este campo sera util para cuando se definen los metadatos
		de forma personalizada, ya que asi tenemos una descripcion rapida para 
		que puede servir si se nos olvida""")
	def __str__(self):
		return f"ServiceMeta[{self.id}]:{self.key} | {self.service.name}"

	
	@classmethod
	def get(cls,id,key,default=None):
		try:
			instance=cls.objects.get(service=id,key=key)
			if settings.JSON_COMPATIBLE:
				value=instance.value
			else:
				value=json.loads(instance.value)

			if instance.encrypted and value:
				from django.apps import apps
				asenzor=apps.get_app_config("asenzor")
				value= asenzor.decode(instance.value,asenzor.get_secret_key())
			return value
		except Exception as e:
			print(e)
		return default
	@classmethod
	def filter(cls,id,key):
		return [json.loads(elem) for elem in cls.objects.filter(service=id,key=key)]
	@classmethod		
	def set(cls,id,key,value,encrypted):
		if encrypted:
			from django.apps import apps
			asenzor=apps.get_app_config("asenzor")
			value=asenzor.endecode(value,asenzor.get_secret_key())
		try:
			instance=cls.objects.get(service=id,key=key)
			if settings.JSON_COMPATIBLE:
				instance[key]=value
			else:
				instance[key]=json.dumps(value)
		except:
			if settings.JSON_COMPATIBLE:
				instance=cls.objects.create(service=id,key=key,value=value)
			else:
				instance=cls.objects.create(service=id,key=key,value=json.dumps(value))

	@classmethod
	def update(cls,id,key,value,array=False):
		"""

		"""
		query=cls.objects.filter(service=id,key=key)
		l=[]
		if type(value)==list or type(value)==tuple:
			if len(query):
				if not array:

					for k,elem in enumerate(query):
						elem[key]=value[k]
						l.append(elem.id)
				else:
					query[0].save()
			else:
				if not array:
					for elem in value:
						if settings.JSON_COMPATIBLE:
							print("||||",value)	
							instance=cls.objects.create(service=id,key=key,value=elem)
						else:
							instance=cls.objects.create(service=id,key=key,value=json.dumps(elem))
						instance.save()
						l.append(instance.id)
				else:
					if settings.JSON_COMPATIBLE:
						print("ssssss",value)
						instance=cls.objects.create(service=id,key=key,value=value)
					else:

						instance=cls.objects.create(service=id,key=key,value=json.dumps(value))
						instance.save()
					


		else:
	

			if len(query):
				if not settings.JSON_COMPATIBLE:
					setattr(query[0],"value",json.dumps(value))
				else:
					setattr(query[0],"value",value)
				query[0].save()
	
				l.append(query[0].id)
			else:
				if not settings.JSON_COMPATIBLE:
					instance=cls.objects.create(service=id,key=key,value=json.dumps(value))
				else:
					instance=cls.objects.create(service=id,key=key,value=value)
				l.append(instance.id)
				instance.save()

		return l

class Plan(models.Model):
	"""
	Grupo de servicios
	"""
	CURRENCY=[
		("usd","Dolar"),
		("eur","Euro"),
	]
	RECURRENCY=[
		("hour","Por Hora"),
		("day","Diario"),
		("15-31","Quience y ultimo"),
		("month","Mensual"),
		("3-month","Trimestral"),
		("6-month","Semestral"),
		("year","Anual"),
	]
	price=models.IntegerField("Precio")
	currency=models.CharField("Moneda",max_length=250)
	recurrency=models.CharField("Recurrencia",max_length=250,
		help_text="Ciclo de facturacion ",
		choices=RECURRENCY)
	description=models.TextField("Descripcion")
	services=models.ManyToManyField("helpdesk.Service")

class Ticket(models.Model):
	PRIORITY=[("up","Alta"),
	        ("middle","Media"),
	        ("low","Baja")]
	STATUS=[
		("opened","Abierto"),
		("pending","Pendiente"),
		("responding","Respondido"),
		("closed","Cerrado"),
	]
	name=models.CharField("Nombre",max_length=250)
	theme=models.ForeignKey("helpdesk.Theme",on_delete=models.CASCADE)
	open_datetime=models.DateTimeField("Fecha apertura")
	closed_datetime=models.DateTimeField("Fecha cierre",blank=True,null=True)
	priority=models.CharField("Prioridad",max_length=250,choices=PRIORITY)
	status=models.CharField("Estatus",max_length=250,choices=STATUS)
	user=models.ForeignKey(User,on_delete=models.CASCADE)
	def __str__(self):
		return f"Ticket[{self.id}]: {self.name} | {self.priority}"

class TicketMessage(models.Model):
	author=models.ForeignKey(User,on_delete=models.CASCADE)
	datetime=models.DateTimeField("Fecha",default=datetime.now)
	ticket=models.ForeignKey("helpdesk.Ticket",on_delete=models.CASCADE)
	content=models.TextField("Contenido",max_length=250,)
	file=models.FileField("Archivo",
		max_length=250,
		upload_to="media/helpdesk/reportes/",
		null=True,blank=True)
