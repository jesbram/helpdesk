from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify

@receiver(pre_save):
def presave_service(sender,instance,**kwargs):
	name=instance.name
	try:
		c=1
		while Service.object.get(name=name):
			name=instance.name+"-"+str(c)
			c+=1
	except:
		instance.name=name
