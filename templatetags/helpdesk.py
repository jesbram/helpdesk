from django import template
from django.apps import apps
from django.http import HttpResponse,HttpResponseRedirect
from django.conf import settings
register = template.Library()
@register.simple_tag
def service_meta(service,name,default=""):
    from ..models import ServiceMeta,User,Service
    from django.utils.html import mark_safe
    if type(service)==Service:
        return mark_safe(ServiceMeta.get(service,name,default))
    else:
        return "'service' debe ser tipo Service"

@register.simple_tag
def url_service(url,app_slug=""):
    from django.utils.html import mark_safe
    from django_middleware_global_request.middleware import get_request
    request=get_request()
    if "domain"  in request.COOKIES and settings.HELPDESK_REDIRECT_SERVICE: 
        return mark_safe("https://"+request.COOKIES["domain"]+url)
    else:
        return mark_safe(app_slug+url)